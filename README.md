Test version of RSA example code from https://briansmith.org/rustdoc/ring/signature/index.html.

Key generation:

```
openssl genpkey -algorithm RSA -outform der -out private_key.der -pkeyopt rsa_keygen_bits:2048
openssl rsa -inform der -outform der -in private_key.der -pubout -out public_key.der
```
