#[cfg(test)]
mod tests {
    extern crate ring;
    extern crate untrusted;

    use self::ring::{rand, signature};

    use std;

    #[test]
    fn test_rsa_signature_roundtrip() {
        let key_bytes_der = untrusted::Input::from(include_bytes!("private_key.der"));
        let key_pair = signature::RSAKeyPair::from_der(key_bytes_der).unwrap();

        // Create a signing state.
        let key_pair = std::sync::Arc::new(key_pair);
        let mut signing_state = signature::RSASigningState::new(key_pair).unwrap();

        // Sign the message "hello, world", using PKCS#1 v1.5 padding and the
        // SHA256 digest algorithm.
        const MESSAGE: &'static [u8] = b"hello, world";
        let rng = rand::SystemRandom::new();
        let mut signature = vec![0; signing_state.key_pair().public_modulus_len()];
        signing_state.sign(&signature::RSA_PKCS1_SHA256, &rng, MESSAGE, &mut signature)
            .unwrap();

        // Verify the signature.
        let public_key_bytes_der = untrusted::Input::from(include_bytes!("public_key.der"));
        let message = untrusted::Input::from(MESSAGE);
        let signature = untrusted::Input::from(&signature);
        signature::verify(&signature::RSA_PKCS1_2048_8192_SHA256,
                          public_key_bytes_der,
                          message,
                          signature)
            .unwrap();
    }
}
